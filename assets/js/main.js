let ctheme = 'light';
var storedTheme = localStorage.getItem('theme');
let topspace;

$(document).ready(function () {
    if (storedTheme === null) {
        $('body').addClass('theme-light');
        setTimeout(function () {
            $('.row').each(function (index) {
                var ele = $(this);
                setTimeout(function () {
                    ele.addClass('row--appear');
                }, 300 * (index + 1));
            })
        }, 400);
    } else {
        $('.bge--1').attr('src', 'assets/img/bge1_' + storedTheme + '.PNG');
        $('.bge--2').attr('src', 'assets/img/bge2_' + storedTheme + '.PNG');
        $('body').addClass('theme-' + storedTheme);
        enter(0);
    }

    updateTopspace();

    let tdScrollbar = window.Scrollbar;
    tdScrollbar.init(document.querySelector('.content'));
    tdScrollbar.getAll()[0].addListener(function (status) {
        var scrollHeight = status.offset;
        scrollRearrange(scrollHeight.y);
        scrollTitle();
    });

    $('.main').mousemove(function (e) {
        var movementStrength = 50;
        var height = movementStrength / $(window).height();
        var width = movementStrength / $(window).width();
        var pageX = e.pageX - ($(window).width() / 2);
        var pageY = e.pageY - ($(window).height() / 2);
        var newvalueX = width * pageX * -1;
        var newvalueY = height * pageY * -1;

        $('.tag').css("transform", "translate(" + (newvalueX * 0.1) + "px, " + (newvalueY * 0.051) + "px)");
    });

    $('.work-content .title').mouseover(function () {
        var workNo = $(this).data('worksection');

        $('.work-content .content[data-worksection="' + workNo + '"]').slideDown();
    });
});

function scrollTitle() {
    for (var i = 2; i >= 0; i--) {
        if ($('.section').eq(i).offset().top < 150) {
            $('.nav-section').removeClass('nav-section--current');
            $('.nav-section').eq(i).addClass('nav-section--current');
            break;
        }
    }
}

function theme(theme) {
    document.getElementsByTagName('body')[0].className = 'theme-' + theme;
    $('.bge--1').attr('src', 'assets/img/bge1_' + theme + '.PNG');
    $('.bge--2').attr('src', 'assets/img/bge2_' + theme + '.PNG');
    ctheme = theme;
    if (storedTheme !== null) localStorage.setItem('theme', ctheme);
}

function enter(delay) {
    if (storedTheme === null) localStorage.setItem('theme', ctheme);

    $('.row').each(function (index) {
        var ele = $(this);
        setTimeout(function () {
            ele.removeClass('row--appear');
        }, 300 * (index + 1));
    });

    setTimeout(function () {
        $('.main').removeClass('front');
        $('.enter').addClass('hidden');
    }, delay);

    setTimeout(function () {
        $('.change').removeClass('change');
    }, delay + 800)
}

function scrollRearrange(scrollheight) {
    $('.bge').each(function (i, e) {
        var speed = parseInt($(e).attr('data-rellax-speed'));
        var factor = -0.05;
        var py = (scrollheight - topspace + 130) * speed * factor;
        var str = 'translate(0,' + py + 'px) rotate(' + (py*0.05) + 'deg)';

        $(e).css({'transform': str, 'opacity': Math.abs(py*0.001)});
    });
}

function updateTopspace() {
    var y = window.innerHeight || window.outerHeight;
    var x = window.innerWidth || window.outerWidth;
    topspace = Math.min(y, Math.floor(x / 16 * 9));
    return true;
}